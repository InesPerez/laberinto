import cv2
from queue import PriorityQueue

Blanco = (255, 255, 255)	# Pared
Negro = (0, 0, 0)			# Pasillo
Rojo = (255, 0, 0)
Verde = (0, 255, 0)
Azul = (0, 0, 255)			# Fin
Morado = (128, 0, 128)		# Camino
Naranja = (255, 165 , 0)	# Inicio



class Nodo:
	def __init__(self, fila, columna, nfilas):
		self.fila = fila
		self.columna = columna
		self.nfilas = nfilas
		self.nodos_vecinos = []
		self.color = Negro

	def get_posicion(self):
		return self.fila, self.columna

	def es_cerrado(self):
		return self.color == Rojo

	def cerrar(self):
		self.color = Rojo

	def es_abierto(self):
		return self.color == Verde

	def abrir(self):
		self.color = Verde

	def es_pared(self):
		return self.color == Blanco

	def crear_pared(self):
		self.color = Blanco

	def es_inicio(self):
		return self.color == Naranja

	def crear_inicio(self):
		self.color = Naranja

	def es_fin(self):
		return self.color == Azul

	def crear_fin(self):
		self.color = Azul

	def reset(self):
		self.color = Negro

	def crear_camino(self):
		self.color = Morado

	def es_camino(self):
		return self.color == Morado


	def actualizar_vecinos(self, grid):
		self.nodos_vecinos = []

		if self.fila > 0 and not grid[self.fila - 1][self.columna].es_pared(): 					# Arriba
			self.nodos_vecinos.append(grid[self.fila - 1][self.columna])

		if self.fila < self.nfilas - 1 and not grid[self.fila + 1][self.columna].es_pared(): 		# Abajo
			self.nodos_vecinos.append(grid[self.fila + 1][self.columna])

		if self.columna > 0 and not grid[self.fila][self.columna - 1].es_pared(): 					# Izquierda
			self.nodos_vecinos.append(grid[self.fila][self.columna - 1])

		if self.columna < self.nfilas - 1 and not grid[self.fila][self.columna + 1].es_pared(): 	# Derecha
			self.nodos_vecinos.append(grid[self.fila][self.columna + 1])



		# HACER Diagonales

#		if self.fila > 0 and not grid[self.fila - 1][self.columna].es_pared(): 					# Arriba
#			self.nodos_vecinos.append(grid[self.fila - 1][self.columna])
#
#		if self.fila < self.nfilas - 1 and not grid[self.fila + 1][self.columna].es_pared(): 		# Abajo
#			self.nodos_vecinos.append(grid[self.fila + 1][self.columna])
#
#		if self.columna > 0 and not grid[self.fila][self.columna - 1].es_pared(): 					# Izquierda
#			self.nodos_vecinos.append(grid[self.fila][self.columna - 1])
#
#		if self.columna < self.nfilas - 1 and not grid[self.fila][self.columna + 1].es_pared(): 	# Derecha
#			self.nodos_vecinos.append(grid[self.fila][self.columna + 1])

	def __lt__(self, other):
		return False


def heuristica(p1, p2):				# función heuristica distancia
	x1, y1 = p1
	x2, y2 = p2
	#return math.sqrt((x1 - y1) ** 2 + (x2 - y2) ** 2)
	return abs(x1 - x2) + abs(y1 - y2)


def reconstruir_camino(came_from, current):
	vector = []
	while current in came_from:
		current = came_from[current]
		y = current.fila
		x = current.columna
		vector.append([x,y])
		# print ("eeeee: "+ str(x) + " "+ str(y))
		current.crear_camino()
	# print(vector)
	return vector



def A_estrella(grid, start, end):
	count = 0
	open_set = PriorityQueue()
	open_set.put((0, count, start))
	came_from = {}
	g_score = {spot: float("inf") for row in grid for spot in row}
	g_score[start] = 0
	f_score = {spot: float("inf") for row in grid for spot in row}
	f_score[start] = heuristica(start.get_posicion(), end.get_posicion())

	open_set_hash = {start}

	while not open_set.empty():

		current = open_set.get()[2]
		open_set_hash.remove(current)

		if current == end:
			camino_vector = reconstruir_camino(came_from, end)
			end.crear_fin()
			return camino_vector

		for neighbor in current.nodos_vecinos:
			temp_g_score = g_score[current] + 1

			if temp_g_score < g_score[neighbor]:
				came_from[neighbor] = current
				g_score[neighbor] = temp_g_score
				f_score[neighbor] = temp_g_score + heuristica(neighbor.get_posicion(), end.get_posicion())
				if neighbor not in open_set_hash:
					count += 1
					open_set.put((f_score[neighbor], count, neighbor))
					open_set_hash.add(neighbor)
					neighbor.abrir()

		if current != start:
			current.cerrar()

	return False


def crear_cuadricula(filas):
	cuadricula = []
	for i in range(filas):					# Crear lista 2D con nodos para formar la cuadricula
		cuadricula.append([])
		for j in range(filas):
			casilla = Nodo(i, j, filas)
			cuadricula[i].append(casilla)
	return cuadricula



def cargar_mapa(numero,cuadricula, filas,frame=None): #si no tenemos
	if frame is None:
		frame = cv2.imread("imagenes/mapa"+str(numero)+".png")
	for row in cuadricula:
		for node in row:
			if node.es_pared():
				node.reset()
	for f in range(filas):
		for c in range(filas):
			if frame[f][c][0] > 100 and frame[f][c][1] > 100 and frame[f][c][2] > 100:
				nodo = cuadricula[c][f]
				nodo.crear_pared()


#def get_waypoints(cuadricula, filas):
#	for f in range(filas):
#		for c in range(filas):
#			nodo = cuadricula[c][f]
#			if nodo.es_camino():
#				print(str(c) +',' + str(f))


def quitar_esquinas(camino):
	anterior = camino[0]
	incremento_anterior = [0,0]
	esquinas = []
	for j in range(len(camino)):
		incremento = [camino[j][i] - anterior[i] for i in range(2)]
		#print("incremento: " + str(incremento))
		diferencia = [incremento[i] - incremento_anterior[i] for i in range(2)]
		if diferencia != [0,0]:
			esquinas.append(j)
		anterior = camino[j]
		incremento_anterior = incremento
		#print( "diferencia: " + str(diferencia)  )
	esquinas.pop(0)
	#print("esquinas: " + str(esquinas))
	#print("camino con esquinas: " + str(camino))
	for k in esquinas[::-1]:
		#print("esquinas "+str(camino[k]))
		del camino[k-1]
		del camino[k-2]
	#print("camino sin esquinas: " + str(camino))
	return camino

def planificacion_ia(yin,xin,yfn,xfn,frame=None,esquinas=True): #frame name of frame


	filas = 32
	cuadricula = crear_cuadricula(filas)

	cargar_mapa(3, cuadricula, filas, frame)

	# posiciones de inicio y fin columna fila
	In = cuadricula[xin][yin]  	# 11 4
	Fn = cuadricula[xfn][yfn]	# 7 19



	In.crear_inicio()
	Fn.crear_fin()

	if In.es_inicio() and Fn.es_fin():
		for row in cuadricula:
			for node in row:
				if node.es_cerrado() or node.es_abierto() or node.es_camino():
					node.reset()
		for row in cuadricula:
			for nodo in row:
				nodo.actualizar_vecinos(cuadricula)

		camino = A_estrella(cuadricula, In, Fn)[::-1]  # invertir orden
		if esquinas == False:
			camino = quitar_esquinas(camino)

		return camino[1:]


"""camino_laberinto = planificacion_ia(4,11,19,7,esquinas=False) # probar con 11 4 7 19

print(camino_laberinto)"""





