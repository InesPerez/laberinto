import pygame
import cv2
#import math
from queue import PriorityQueue

Ancho = 800			                                    	#ancho de la ventana
Ancho_ventana = 950
Ventana = pygame.display.set_mode((Ancho_ventana, Ancho))			#SETUP display
pygame.display.set_caption("Resolución de laberintos con A*")	#título de la ventana


Blanco = (255, 255, 255)
Negro = (0, 0, 0)
Rojo = (255, 0, 0)
Verde = (0, 255, 0)
Azul = (0, 0, 255)
Amarillo = (255, 255, 0)
Morado = (128, 0, 128)
Naranja = (255, 165 , 0)
Gris = (128, 128, 128)


class Nodo:
	def __init__(self, fila, columna, ancho, nfilas):
		self.fila = fila
		self.columna = columna
		self.ancho = ancho
		self.nfilas = nfilas
		self.x = fila    * ancho	# coordenada x en la pantalla
		self.y = columna * ancho	# coordenada y en la pantalla
		self.nodos_vecinos = []
		self.color = Negro

	def get_posicion(self):
		return self.fila, self.columna

	def es_cerrado(self):
		return self.color == Rojo

	def cerrar(self):
		self.color = Rojo

	def es_abierto(self):
		return self.color == Verde

	def abrir(self):
		self.color = Verde

	def es_pared(self):
		return self.color == Blanco

	def crear_pared(self):
		self.color = Blanco

	def es_inicio(self):
		return self.color == Naranja

	def crear_inicio(self):
		self.color = Naranja

	def es_fin(self):
		return self.color == Azul

	def crear_fin(self):
		self.color = Azul

	def reset(self):
		self.color = Negro

	def crear_camino(self):
		self.color = Morado

	def es_camino(self):
		return self.color == Morado

	def pintar_casilla(self, ventana):
		pygame.draw.rect(ventana, self.color, (self.x, self.y, self.ancho, self.ancho))

	def actualizar_vecinos(self, grid):
		self.nodos_vecinos = []

		if self.fila > 0 and not grid[self.fila - 1][self.columna].es_pared(): 					# Arriba
			self.nodos_vecinos.append(grid[self.fila - 1][self.columna])

		if self.fila < self.nfilas - 1 and not grid[self.fila + 1][self.columna].es_pared(): 		# Abajo
			self.nodos_vecinos.append(grid[self.fila + 1][self.columna])

		if self.columna > 0 and not grid[self.fila][self.columna - 1].es_pared(): 					# Izquierda
			self.nodos_vecinos.append(grid[self.fila][self.columna - 1])

		if self.columna < self.nfilas - 1 and not grid[self.fila][self.columna + 1].es_pared(): 	# Derecha
			self.nodos_vecinos.append(grid[self.fila][self.columna + 1])



		# HACER Diagonales

#		if self.fila > 0 and not grid[self.fila - 1][self.columna].es_pared(): 					# Arriba
#			self.nodos_vecinos.append(grid[self.fila - 1][self.columna])
#
#		if self.fila < self.nfilas - 1 and not grid[self.fila + 1][self.columna].es_pared(): 		# Abajo
#			self.nodos_vecinos.append(grid[self.fila + 1][self.columna])
#
#		if self.columna > 0 and not grid[self.fila][self.columna - 1].es_pared(): 					# Izquierda
#			self.nodos_vecinos.append(grid[self.fila][self.columna - 1])
#
#		if self.columna < self.nfilas - 1 and not grid[self.fila][self.columna + 1].es_pared(): 	# Derecha
#			self.nodos_vecinos.append(grid[self.fila][self.columna + 1])

	def __lt__(self, other):
		return False


def heuristica(p1, p2):				# función heuristica distancia
	x1, y1 = p1
	x2, y2 = p2
	#return math.sqrt((x1 - y1) ** 2 + (x2 - y2) ** 2)
	return abs(x1 - x2) + abs(y1 - y2)


def reconstruir_camino(came_from, current):
	vector = []
	while current in came_from:
		current = came_from[current]
		y = current.fila
		x = current.columna
		vector.append([x,y])
		# print ("eeeee: "+ str(x) + " "+ str(y))
		current.crear_camino()
	# print(vector)
	return vector


def A_estrella(draw, grid, start, end):
	count = 0
	open_set = PriorityQueue()
	open_set.put((0, count, start))
	came_from = {}
	g_score = {spot: float("inf") for row in grid for spot in row}
	g_score[start] = 0
	f_score = {spot: float("inf") for row in grid for spot in row}
	f_score[start] = heuristica(start.get_posicion(), end.get_posicion())

	open_set_hash = {start}

	while not open_set.empty():
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				pygame.quit()

		current = open_set.get()[2]
		open_set_hash.remove(current)

		if current == end:
			camino_vector = reconstruir_camino(came_from, end)
			end.crear_fin()
			#print("Con esquinas: " + str((camino_vector[::-1])[1:]))
			#print("Sin esquinas: " + str(quitar_esquinas(camino_vector[::-1])[1:]))
			#return True
			return camino_vector

		for neighbor in current.nodos_vecinos:
			temp_g_score = g_score[current] + 1

			if temp_g_score < g_score[neighbor]:
				came_from[neighbor] = current
				g_score[neighbor] = temp_g_score
				f_score[neighbor] = temp_g_score + heuristica(neighbor.get_posicion(), end.get_posicion())
				if neighbor not in open_set_hash:
					count += 1
					open_set.put((f_score[neighbor], count, neighbor))
					open_set_hash.add(neighbor)
					neighbor.abrir()

		draw()

		if current != start:
			current.cerrar()

	return False


def crear_cuadricula(filas, ancho):
	cuadricula = []
	separacion = ancho // filas				# // division entera => separacion entre filas
	for i in range(filas):					# Crear lista 2D con nodos para formar la cuadricula
		cuadricula.append([])
		for j in range(filas):
			casilla = Nodo(i, j, separacion, filas)
			cuadricula[i].append(casilla)
	return cuadricula


def dibujar_cuadricula(ventana, filas, ancho):
	separacion = ancho // filas
	for i in range(filas):
		pygame.draw.line(ventana, Gris, (0, i * separacion), (ancho, i * separacion)) 		# dibujar lineas horizontales
		for j in range(filas+1):
			pygame.draw.line(ventana, Gris, (j * separacion, 0), (j * separacion, ancho))	# dibujar lineas verticales


def dibujar_todo(ventana, cuadricula, filas, ancho):
	#ventana.fill(Blanco)								# Fondo blanco
	pygame.draw.rect(Ventana,(255,255,255), pygame.Rect(0, 0, 800, 800))
	for fila in cuadricula:
		for nodo in fila:
			nodo.pintar_casilla(ventana)				# Pintar el color de cada casilla

	dibujar_cuadricula(ventana, filas, ancho)  # Dibujar cuadrícula

	pygame.display.update()


def get_posicion_click(posicion, filas, ancho):	# funcion para saber fila y columna de una casilla que se ha hecho click
	separacion = ancho // filas
	x, y = posicion
	fila = x // separacion
	columna = y // separacion

	return fila, columna

def cargar_mapa(numero,cuadricula, filas,frame=None):
	if frame is None:
		frame = cv2.imread("imagenes/mapa"+str(numero)+".png")
	for row in cuadricula:
		for node in row:
			if node.es_pared():
				node.reset()
	for f in range(filas):
		for c in range(filas):
			if frame[f][c][0] > 100 and frame[f][c][1] > 100 and frame[f][c][2] > 100:
				nodo = cuadricula[c][f]
				nodo.crear_pared()


def set_waypoints(cuadricula, camino):
	for row in cuadricula:
		for node in row:
			if node.es_cerrado() or node.es_abierto() or node.es_camino():
				node.reset()
	for i in range(len(camino)):
		nodo = cuadricula[camino[i][1]][camino[i][0]]
		nodo.crear_camino()


def quitar_esquinas(camino):
	anterior = camino[0]
	incremento_anterior = [0,0]
	esquinas = []
	for j in range(len(camino)):
		incremento = [camino[j][i] - anterior[i] for i in range(2)]
		#print("incremento: " + str(incremento))
		diferencia = [incremento[i] - incremento_anterior[i] for i in range(2)]
		if diferencia != [0,0]:
			esquinas.append(j)
		anterior = camino[j]
		incremento_anterior = incremento
		#print( "diferencia: " + str(diferencia)  )
	esquinas.pop(0)
	#print("esquinas: " + str(esquinas))
	#print("camino con esquinas: " + str(camino))
	for k in esquinas[::-1]:
		#print("esquinas "+str(camino[k]))
		del camino[k-1]
		del camino[k-2]
	#print("camino sin esquinas: " + str(camino))
	return camino

def planificacion_ia(ventana, ancho,esquinas=False):

	# INTERFAZ -----------------------------------------------------------------

	desfase_x = 805
	desfase_y = 30

	# Initialize
	pygame.init()
	pygame.draw.rect(Ventana, (255, 255, 255), pygame.Rect(800, 0, 300, 800))

	font = pygame.font.SysFont('Calibri', 26, bold=True, italic=False)
	font2 = pygame.font.SysFont('Calibri', 22, bold=True, italic=False)

	# Texto Inicial
	font_text_ini = pygame.font.Font('C:\Windows\Fonts\Calibri.ttf', 22)



	# Inicio Parameters
	Inicio = font.render('Inicio', True, Gris)
	Inicio_pos = (desfase_x + 25, desfase_y + 30)
	Inicio_status = False
	Inicio_rect = Inicio.get_rect()
	Inicio_rect.x = Inicio_pos[0]
	Inicio_rect.y = Inicio_pos[1]

	# Fin Parameters
	Fin = font.render('Fin', True, Gris)
	Fin_pos = (desfase_x + 25, desfase_y + 60)
	Fin_status = False
	Fin_rect = Fin.get_rect()
	Fin_rect.x = Fin_pos[0]
	Fin_rect.y = Fin_pos[1]

	# Pared Parameters
	Pared = font.render('Pared', True, Gris)
	Pared_pos = (desfase_x + 25, desfase_y + 90)
	Pared_status = False
	Pared_rect = Pared.get_rect()
	Pared_rect.x = Pared_pos[0]
	Pared_rect.y = Pared_pos[1]

	# Borrar Parameters
	Borrar = font.render('Borrar', True, Gris)
	Borrar_pos = (desfase_x + 25, desfase_y + 120)
	Borrar_status = False
	Borrar_rect = Borrar.get_rect()
	Borrar_rect.x = Borrar_pos[0]
	Borrar_rect.y = Borrar_pos[1]




	# Limpiar escenario
	Limpiar = font2.render('Limpiar', True, Gris)
	Limpiar_pos = (desfase_x + 20, desfase_y + 340)
	Limpiar_rect = Limpiar.get_rect()
	Limpiar_rect.x = Limpiar_pos[0]
	Limpiar_rect.y = Limpiar_pos[1]

	# Borrar Todo
	Borrar_todo = font2.render('Borrar Todo', True, Gris)
	Borrar_todo_pos = (desfase_x + 20, desfase_y + 370)
	Borrar_todo_rect = Borrar_todo.get_rect()
	Borrar_todo_rect.x = Borrar_todo_pos[0]
	Borrar_todo_rect.y = Borrar_todo_pos[1]

	# Ejecutar
	Ejecutar = font2.render('Ejecutar', True, Gris)
	Ejecutar_pos = (desfase_x + 20, desfase_y + 400)
	Ejecutar_rect = Ejecutar.get_rect()
	Ejecutar_rect.x = Ejecutar_pos[0]
	Ejecutar_rect.y = Ejecutar_pos[1]

	# Mapa 1
	Mapa1 = pygame.image.load("imagenes/Mapa1.png")
	Mapa1_pos = (desfase_x + 30, desfase_y + 480)
	Mapa1_rect = Mapa1.get_rect()
	Mapa1_rect.x = Mapa1_pos[0]
	Mapa1_rect.y = Mapa1_pos[1]

	# Mapa 2
	Mapa2 = pygame.image.load("imagenes/Mapa3.png")
	Mapa2_pos = (desfase_x + 80, desfase_y + 480)
	Mapa2_rect = Mapa2.get_rect()
	Mapa2_rect.x = Mapa2_pos[0]
	Mapa2_rect.y = Mapa2_pos[1]

	clock = pygame.time.Clock()
	clock.tick(60)  # 60 fps

	# ---------------------------------------------------------------------------------

	filas = 32
	cuadricula = crear_cuadricula(filas, ancho)

	In = cuadricula[-1][-1]
	Fn = cuadricula[-1][-1]


	ejecutar = True

	while ejecutar:
		dibujar_todo(ventana, cuadricula, filas, ancho)
		for event in pygame.event.get():
			if event.type == pygame.QUIT:		# Si se cierra la ventana
				ejecutar = False

			if pygame.mouse.get_pressed()[0]: 							# Hacer click izquierdo
				posicion = pygame.mouse.get_pos()
				if(posicion[0]<=800 and posicion[1]<=800):				# dentro de la cuadricula
					fila, columna = get_posicion_click(posicion, filas, ancho)
					nodo = cuadricula[fila][columna]

					if (Inicio_status == True):
						if In:
							In.reset()
						In = nodo
						In.crear_inicio()
						print("Inicio: ("+ str(In.columna) + "," + str(In.fila) + ")")

					if (Fin_status == True):
						if Fn:
							Fn.reset()
						Fn = nodo
						Fn.crear_fin()
						print("Fin: (" + str(Fn.columna) + "," + str(Fn.fila) + ")")

					if (Pared_status == True):
						nodo.crear_pared()

					if (Borrar_status == True):
						nodo.reset()


				else:														# Interfaz
					if (Inicio_rect.collidepoint(posicion[0],posicion[1])):
						if (Inicio_status):
							Inicio_status = False
							print("Inicio OFF")
						else:
							Inicio_status = True
							Fin_status = False
							Pared_status = False
							Borrar_status = False
							print("Inicio ON")

					if (Fin_rect.collidepoint(posicion[0],posicion[1])):
						if (Fin_status):
							Fin_status = False
							print("Fin OFF")
						else:
							Fin_status = True
							Inicio_status = False
							Pared_status = False
							Borrar_status = False
							print("Fin ON")


					if (Pared_rect.collidepoint(posicion[0],posicion[1])):
						if (Pared_status):
							Pared_status = False
							print("Pared OFF")
						else:
							Pared_status = True
							Fin_status = False
							Inicio_status = False
							Borrar_status = False
							print("Pared ON")

					if (Borrar_rect.collidepoint(posicion[0],posicion[1])):
						if (Borrar_status):
							Borrar_status = False
							print("Borrar OFF")
						else:
							Borrar_status = True
							Fin_status = False
							Pared_status = False
							Inicio_status = False
							print("Borrar ON")


					if (Limpiar_rect.collidepoint(posicion[0],posicion[1])):
						print("Reset búsqueda")
						for row in cuadricula:
							for node in row:
								if node.es_cerrado() or node.es_abierto() or node.es_camino():
									node.reset()

					if (Borrar_todo_rect.collidepoint(posicion[0],posicion[1])):
						print("BORRAR TODO")
						cuadricula = crear_cuadricula(filas, ancho)

					if (Ejecutar_rect.collidepoint(posicion[0],posicion[1])):
						print("Ejecutar")
						if In.es_inicio() and Fn.es_fin():
							for row in cuadricula:
								for node in row:
									if node.es_cerrado() or node.es_abierto() or node.es_camino():
										node.reset()
							for row in cuadricula:
								for nodo in row:
									nodo.actualizar_vecinos(cuadricula)

							camino = A_estrella(lambda: dibujar_todo(Ventana, cuadricula, filas, ancho), cuadricula, In, Fn)[::-1]
							print("Con esquinas: " + str(camino[1:]))
							if esquinas == False:
								camino = quitar_esquinas(camino)
							print("Sin esquinas: " + str(camino[1:]))
							set_waypoints(cuadricula, camino)

					if (Mapa1_rect.collidepoint(posicion[0], posicion[1])):
						print("Cargar Mapa 1")
						In = cuadricula[-1][-1]
						Fn = cuadricula[-1][-1]
						for row in cuadricula:
							for node in row:
								node.reset()
						cargar_mapa(1,cuadricula,filas)

					if (Mapa2_rect.collidepoint(posicion[0], posicion[1])):
						print("Cargar Mapa 2")
						In = cuadricula[-1][-1]
						Fn = cuadricula[-1][-1]
						for row in cuadricula:
							for node in row:
									node.reset()
						cargar_mapa(3,cuadricula,filas)



		if (Inicio_status == True):
			Inicio = font.render('Inicio', True, (0, 255, 0))
		else:
			Inicio = font.render('Inicio', True, Gris)

		if (Fin_status == True):
			Fin = font.render('Fin', True, (0, 255, 0))
		else:
			Fin = font.render('Fin', True, Gris)

		if (Pared_status == True):
			Pared = font.render('Pared', True, (0, 255, 0))
		else:
			Pared = font.render('Pared', True, Gris)

		if (Borrar_status == True):
			Borrar = font.render('Borrar', True, (0, 255, 0))
		else:
			Borrar = font.render('Borrar', True, Gris)



		# Blit & Flip
		#Ventana.blit(background, (0, 0))
		pygame.draw.rect(Ventana, (255, 255, 255), pygame.Rect(801, 0, 300, 800))


		Ventana.blit(Inicio, Inicio_pos)
		Ventana.blit(Fin, Fin_pos)
		Ventana.blit(Pared, Pared_pos)
		Ventana.blit(Borrar, Borrar_pos)

		Ventana.blit(Limpiar, Limpiar_pos)
		Ventana.blit(Borrar_todo, Borrar_todo_pos)
		Ventana.blit(Ejecutar, Ejecutar_pos)
		Ventana.blit(Mapa1, Mapa1_pos)
		Ventana.blit(Mapa2, Mapa2_pos)

		pygame.display.flip()  # repintar

	pygame.quit()

planificacion_ia(Ventana, Ancho,esquinas=True)
