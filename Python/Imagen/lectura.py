import numpy as np
import cv2 as cv
from socket import *
import os
import sys

class Imagen:
    name="Imagen"
    resolucion = 32
    lab_size=25
    addr = ("127.0.0.1", 8080)

    def umbralizar(self,img,r_ini, r_fin,show=0):#rango inicial y final
        thresh = cv.inRange(img, r_ini, r_fin)
        return thresh
        
    def puntos(self,box,show=0):
        x=[]
        y=[]
        p=box[0]
        for p in box:
            x.append(p[0])
            y.append(p[1])
        x=sorted(x)
        y=sorted(y)
        if show:
            print('Vertices de la caja',box)
            print('x: ',x)
            print('y: ',y)
        
        return (x,y)

    def recorte(self,box,img,show_cmd=0):
        (x,y) = self.puntos(box,show_cmd)
        topy=y[1]
        topx=x[1]
        bottomy=y[2]
        bottomx=x[2]
        if show_cmd:
            print('rango x: ',topx,'-',bottomx)
            print('rango y: ',topy,'-',bottomy)

        recorte = img[topy:bottomy,topx:bottomx]
        return recorte

    def circulo(self,img,show=0):
        output = img.copy()
        gray_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        blur = cv.medianBlur(gray_img, 5)
        # Find circles
        circles = cv.HoughCircles(blur, cv.HOUGH_GRADIENT,1, 5,param1=200, param2=15, minRadius=5, maxRadius=20)
        detected_circles = np.uint16(np.around(circles))

        for (x, y, r) in detected_circles[0, :]:
            cv.circle(output, (x, y), r, (0, 255, 0), 3)
            cv.circle(output, (x, y), 2, (255, 0, 0), 3)
        if show:
            cv.imshow("Original Image", img)
            cv.imshow("Output", output)
        return detected_circles

    def dibujo_contorno(self,contornos=None,imagen=None,imagen_box=None,show=0):
        if contornos is None:
            contornos=self.contours
        #dibujo del contorno
        cont_max = max(contornos, key = cv.contourArea)
        
        if imagen is not None:
            contorno=imagen
            cv.drawContours(contorno,[cont_max],0,(0,0,0),2)
            if show:
                cv.imshow("contorno", contorno)
                
        # obtencion del rectangulo girado del contorno
        rot_rect = cv.minAreaRect(cont_max)
        box = cv.boxPoints(rot_rect)
        box = np.int0(box)
        if imagen_box is not None:
            contorno_box=imagen_box
            # dibujo del rectangulo girado en la imagen
            cv.drawContours(contorno_box,[box],0,(0,255,0),2)
            if show:
                cv.imshow("caja", contorno_box)
        return box

    def obt_contorno(self,img,low,up,elipse_sz=(5,5),show=0):
        # Rango de detección de las paredes blancas
        if isinstance(low,int):
            lower = (low,low,low)
        else: lower=low
        if isinstance(up,int):
            upper = (up,up,up)
        else: upper = up
        if show:
            print(lower)
            print(upper)
            
        thresh = self.umbralizar(img, lower, upper)

        # suavizado de los bordes
        kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, elipse_sz)
        clean = cv.morphologyEx(thresh, cv.MORPH_CLOSE, kernel)

        # Obtencion de contornos
        contours = cv.findContours(clean, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
        contours = contours[0] if len(contours) == 2 else contours[1]

        return (contours,clean,thresh)

    def ventana_grande(self,nombre,img):
        cv.namedWindow(nombre, cv.WINDOW_NORMAL)
        cv.imshow(nombre, img)

    def pelota(self,show_mask=0,show_circulo=0,show_cmd=0):

        img=self.img
        camino_mask=self.clean_rec
        box=self.box

        img_rec = self.recorte(box,img)
        paredes_mask = cv.bitwise_not(camino_mask)
        sin_paredes=cv.bitwise_and(img_rec,img_rec,mask=paredes_mask)

        mask_red=self.umbralizar(sin_paredes,(0,0,0),(100,100,100))
        mask_pelota = cv.bitwise_not(mask_red)
        
        if show_mask:
            cv.imshow('mascara rojo',mask_pelota)
            
        img_pelota = cv.bitwise_and(sin_paredes,sin_paredes,mask=mask_pelota)
        
        cr=self.circulo(img_pelota,show_circulo)
        parametros=cr[0, :][0]

        #Ajuste de resolucion
        (h,w,_)=img_pelota.shape
        
        cx_real=round(parametros[0]/w*self.resolucion*100)
        cy_real=round(parametros[1]/h*self.resolucion*100)

        
        cx = round(parametros[0]/w*self.resolucion)
        cy = round(parametros[1]/h*self.resolucion)
        
        if show_cmd: print(f"centro pelota cx: {cx} cy: {cy}")
        
        return (cx_real,cy_real,cx,cy)


    def destino(self,show_mask=0,show_destino=0,show_cmd=0):
        img=self.img
        box=self.box
        camino_mask=self.clean_rec
        
        cv.imshow('camino_mask',camino_mask)
        
        img_rec = self.recorte(box,img)
        paredes_mask = cv.bitwise_not(camino_mask)
        sin_paredes=cv.bitwise_and(img_rec,img_rec,mask=paredes_mask)

        cv.imshow('sin_paredes',sin_paredes)

        img_destino=self.umbralizar(sin_paredes.copy(),0,(120,90,80))#(120,80,80))#(120,80,70))#(70,70,70))#(70,70,50))#(70,80,50))#(70,100,50))#(0, 50, 0),(50,100,50))
        img_destino=cv.bitwise_not(img_destino)
        img_destino=cv.cvtColor(img_destino, cv.COLOR_GRAY2BGR)
        
        #(contours,clean,thresh)=self.obt_contorno(img_destino,0,50,elipse_sz=(12,12))
        (contours,_,_)=self.obt_contorno(img_destino,0,50,elipse_sz=(12,12))

        
        box=self.dibujo_contorno(contornos=contours,imagen_box=img_destino)

        if show_mask:
            img_centro= img_destino.copy()
            cv.imshow('destino verde',img_centro)
            
        
        (cx,cy)=self.centro(box,img_destino,show_cmd)

        #Ajuste de resolucion
        (h,w,_)=img_destino.shape

        
        dest_cx = round(cx/w*self.resolucion)
        dest_cy = round(cy/h*self.resolucion)
        
        if show_destino:
            cv.imshow('destino',img_destino)

        return (dest_cx,dest_cy)

    def centro (self,contour,img,show_cmd=0):
        M = cv.moments(contour)
        if M['m00'] != 0:
            cx = int(M['m10']/M['m00'])
            cy = int(M['m01']/M['m00'])
            cv.drawContours(img, [contour], -1, (0, 255, 255), 2)
            cv.circle(img, (cx, cy), 7, (0, 0, 255), -1)
            cv.putText(img, "center", (cx - 20, cy - 20),cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)
            if show_cmd: print(f"x: {cx} y: {cy}")
        return (cx,cy)

    def init_socket(self):
        self.pelotaSocket = socket(AF_INET, SOCK_DGRAM)
        self.pelotaSocket.settimeout(1)

        #return clientSocket


    def enviar(self,socket,eje,valor):
        datos=''
        if valor<100:
            if valor<10:    
                datos=datos+'0'
            datos=datos+'0'
        datos=datos+str(valor)
        
        mensaje=eje+'_'+datos
        socket.sendto(mensaje.encode(), self.addr)


    def __init__(self,show_text=0):
        self.stop=0
        print(os.getcwd())
        self.init_socket()
        self.cap=cv.VideoCapture(1)
        
        if not self.cap.isOpened():
            print("No se puede abrir la camara")
            exit()

    def obtencion_imagen(self,cap,path='./ficheros_prueba/laberinto_webcam.jpg',show_cmd=0):
        # Captura
        if cap:
            ret, frame = self.cap.read()
            if not ret:
                print("No se recibe  captura.")
                self.stop=1
            cv.imshow('camara',frame)
        else:
            frame = cv.imread(path)
            
        #Escalado
        """scale_percent = 40 # percent of original size
        width = int(frame.shape[1] * scale_percent / 100)
        height = int(frame.shape[0] * scale_percent / 100)
        dim = (width, height)
        img = cv.resize(frame, dim, interpolation = cv.INTER_AREA)"""
        
        self.img=frame.copy()
        (self.contours,self.clean,self.thresh)=self.obt_contorno(self.img,150,255,show=show_cmd)
        
        #cv.imshow("thresh", self.thresh)#umbralizacion
        #cv.imshow("clean", self.clean)#mascara

    def preparar_imagen(self):
        self.box=self.dibujo_contorno(imagen_box=self.img)

        #Recorte imagen
        self.clean_rec = self.recorte(self.box,self.clean)

    def generar_imagen(self,cr_x,cr_y,dest_x,dest_y,show=0):
        #Cambio resolucion 32x32
        (h,w)=self.clean_rec.shape
        dim = (self.resolucion,self.resolucion)
        resized_ia = cv.resize(self.clean_rec, dim, interpolation = cv.INTER_AREA)

        #Claridad de la imagen
        lower = (100,100,100)
        upper = (255,255,255)
        ia = cv.cvtColor(resized_ia, cv.COLOR_GRAY2BGR)
        ia = self.umbralizar(ia,lower,upper,1)
        ia=cv.merge([ia,ia,ia])# bgr de 3 canales

        #origen
        cv.rectangle(ia, pt1=(cr_x, cr_y), pt2=(cr_x, cr_y), color=(0,0,255), thickness=-1)
        #destino
        cv.rectangle(ia, pt1=(dest_x, dest_y), pt2=(dest_x, dest_y), color=(0,255,0), thickness=-1)

        if show: self.ventana_grande("imagen ia", ia)

        self.ia=ia
        return ia
        
"""try:
    show_cmd=sys.argv[1]
except:
    show_cmd=0

vision=Imagen(show_text=show_cmd)
        
while not vision.stop:
    camara=1
    vision.obtencion_imagen(cap=camara)        
    if len(vision.contours)>0:
        try:
            vision.preparar_imagen()
            
            #detección de la pelota
            (cx_real,cy_real,cr_x,cr_y)=vision.pelota(show_mask=1,show_circulo=0,show_cmd=show_cmd)

            vision.enviar(vision.pelotaSocket,'x',cx_real)
            vision.enviar(vision.pelotaSocket,'y',cy_real)

            #if se pide planificacion:    
            #deteccion destino
            (dest_x,dest_y)=vision.destino(show_mask=1,show_destino=1,show_cmd=show_cmd)
            ia=vision.generar_imagen(cr_x,cr_y,dest_x,dest_y,show=1)            

        except Exception as e:
            print(e)
        except KeyboardInterrupt:
            break

        if cv.waitKey(1) == ord('q'):
            break
        
#liberamos recursos
if camara: vision.cap.release()
cv.destroyAllWindows()"""
