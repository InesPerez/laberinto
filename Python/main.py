from Imagen.lectura import Imagen
from IA.planificacion_lab import *
import cv2 as cv
import serial
import time

def enviar_punto(punto,menor_que=10):
    print(punto)
    #reinicio=1
    #while reinicio:
    dato=""
    for coordenada in punto:
        if coordenada<menor_que:
            dato=dato+'0'
        dato=dato+str(coordenada)
    return dato
        #time.sleep(0.1)
        #respuesta = ser.read(size=1)
       # if respuesta==bytes(1, 'utf-8'):
        #    print("Si")
        #    reinicio=0
        #else:
        #    print("No")
        #    reinicio=1


try:
    show_cmd=sys.argv[1]
except:
    show_cmd=0

vision=Imagen(show_text=show_cmd)
puntos_maximos=30
planificar=1

serial_posicion = serial.Serial('COM4', 115200, timeout=200)
serial_posicion.close()
serial_posicion.open()

serial_trayectoria_com = serial.Serial('COM6', 115200, timeout=400)
serial_trayectoria_com.close()
serial_trayectoria_com.open()

print("Inicio")
#serial_posicion.write(bytes('99999999', 'utf-8'))
     
while not vision.stop:
    camara=1
    vision.obtencion_imagen(cap=camara,path='Imagen/ficheros_prueba/laberinto_webcam.jpg')        
    if len(vision.contours)>0:
        try:
            
            vision.preparar_imagen()
            #detección de la pelota
            (cx_real,cy_real,cr_x,cr_y)=vision.pelota(show_mask=0,show_circulo=0,show_cmd=show_cmd)
            #deteccion destino
            #(dest_x,dest_y)=vision.destino(show_mask=0,show_destino=1,show_cmd=show_cmd)
            (dest_x,dest_y)=(3,19)
            ia=vision.generar_imagen(cr_x,cr_y,dest_x,dest_y,show=1)
            cv.imwrite('./IA/imagenes/mapa1.png',ia)

            
            #print("Posición Actual")
            punto=enviar_punto((cx_real,cy_real),1000)
            punto=bytes(punto, 'utf-8')
            print(punto)
            serial_posicion.write(punto)
            time.sleep(0.2)
            #print("Fin posicón Actual")

            #respuesta=None
            #respuesta = serial_trayectoria_com.read(size=1)
            #time.sleep(0.2)
            #print(respuesta)
            #if respuesta==bytes('1', 'utf-8'):
            #    planificar=1
            #time.sleep(5)
            
            while planificar:#respuesta==bytes('1', 'utf-8'):
                """punto= (cx_real,cy_real)
                dato=bytes('0', 'utf-8')
                ser.write(dato)
                respuesta = ser.read(size=1)
                while respuesta!=bytes('1', 'utf-8'):
                    respuesta = ser.read(size=1)
                enviar_punto(punto)
                dato=bytes('9', 'utf-8')
                ser.write(dato)
                print("Posicion actual enviada")
            else:
                if respuesta==bytes('2', 'utf-8'):"""
            #planificacion
             #   planificar=1
            #if planificar:
                try:
                    print("Planificacion")
                    planificar=0           
                    camino_laberinto = planificacion_ia(cr_x,cr_y,dest_x,dest_y,ia)
                        
                    #print(camino_laberinto)
                    #dato=bytes('0', 'utf-8')
                    #ser.write(dato)
                    #respuesta = ser.read(size=1)
                    #while respuesta!=bytes('1', 'utf-8'):
                     #   respuesta = ser.read(size=1)
                    trayectoria=''
                    for punto in camino_laberinto:
                        dato=enviar_punto(punto)
                        trayectoria=trayectoria+dato
                    relleno=puntos_maximos - round(len(trayectoria)/4)
                    print(relleno)
                    for i in range(relleno*4):
                        trayectoria=trayectoria+'9'
                    trayectoria=bytes(trayectoria, 'utf-8')
                    serial_trayectoria_com.write(trayectoria)
                    print(trayectoria)
                    print(len(trayectoria))
                    time.sleep(1)
                    serial_trayectoria_com.write(trayectoria)
                    time.sleep(5)
                except Exception:
                    planificar=1
                    
                
                #respuesta = ser.read(size=1)
                #while respuesta!=bytes('9', 'utf-8'):
                #    respuesta = ser.read(size=1)
                #    print(respuesta)
                #print("Fin")
                
            
        except Exception as e:
         #   print(e)
            continue
            
        except KeyboardInterrupt:
            break

        if cv.waitKey(1) == ord('q'):
            break
        
        if cv.waitKey(1) == ord('r'):
            print('Reinicio')
            continue;
        
#liberamos recursos
if camara: vision.cap.release()
#cv.destroyAllWindows()
