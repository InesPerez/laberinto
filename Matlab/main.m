clear clc
clear all

addpath('.\Imagen\')
echoudp('on',8080)

% try
%     delete(gcp('nocreate'))
% end

mycluster=parpool('local',2);
try
parfor (i=1:2,2)
    %Vision
    if i==1
        cd('..\Python\Imagen\')
        pyrunfile('lectura.py')
    %Adquisición de datos
    elseif i==2
        u=udpport("IPV4",LocalHost="127.0.0.1",LocalPort=8080);
        try
            info=true;
            while info
                try
                    data=datos(u);
                    disp(data)
                catch
                    info=false;
                end
            end
        catch
            flush(u,"output");
        end
    end
end
catch e
    e.message
    e.cause
end
echoudp('off')
delete(gcp('nocreate'))