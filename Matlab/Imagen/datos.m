function coordenadas=datos(u)
coordenadas=[];
datos1 = read(u,5,"char");
datos2 = read(u,5,"char");

info1=split(datos1,'_');
info2=split(datos2,'_');
coordenadas=read_info(info1,coordenadas);
coordenadas=read_info(info2,coordenadas);
end

function dato=read_info(info,dato)
    if strcmp(info{1},'x')
    dato(1)=str2double(info{2});
elseif strcmp(info{1},'y')
    dato(2)=str2double(info{2});
    end
end
