################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Librerias/ILI3941/ILI9341.c 

C_DEPS += \
./Librerias/ILI3941/ILI9341.d 

OBJS += \
./Librerias/ILI3941/ILI9341.o 


# Each subdirectory must supply rules for building sources it contributes
Librerias/ILI3941/%.o Librerias/ILI3941/%.su: ../Librerias/ILI3941/%.c Librerias/ILI3941/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F407xx -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I"C:/Users/inesm/Desktop/Laberinto/STM32Cube IDE/Laberinto2/Librerias/ILI3941" -I../USB_HOST/App -I../USB_HOST/Target -I../Middlewares/ST/STM32_USB_Host_Library/Core/Inc -I../Middlewares/ST/STM32_USB_Host_Library/Class/CDC/Inc -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Librerias-2f-ILI3941

clean-Librerias-2f-ILI3941:
	-$(RM) ./Librerias/ILI3941/ILI9341.d ./Librerias/ILI3941/ILI9341.o ./Librerias/ILI3941/ILI9341.su

.PHONY: clean-Librerias-2f-ILI3941

