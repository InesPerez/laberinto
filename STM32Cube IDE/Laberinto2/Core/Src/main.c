/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_host.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdlib.h"
#include "math.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
 I2C_HandleTypeDef hi2c1;

I2S_HandleTypeDef hi2s3;

SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;

UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;
DMA_HandleTypeDef hdma_usart2_rx;
DMA_HandleTypeDef hdma_usart3_rx;

/* USER CODE BEGIN PV */
uint8_t inicio=0;
#define Npuntos 30

uint8_t Rx_data2[8];
uint8_t Rx_data3[Npuntos*4];
uint8_t Tx_data[1];

uint8_t puntos[Npuntos+2][2];
int n=0;
int trayectoria_ok=0;
float posicion[2];
int esquina;

int xd,yd;
float xdelta,ydelta;
float xanterior, yanterior;
int numfilas=0;
int tol=0;int tol2=0;int tol3=0;int tol4=0;
uint8_t delay=70;
uint8_t next=0;
uint8_t next_i=0;


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_I2S3_Init(void);
static void MX_SPI1_Init(void);
static void MX_TIM2_Init(void);
static void MX_DMA_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM1_Init(void);
static void MX_USART3_UART_Init(void);
void MX_USB_HOST_Process(void);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


/*void Rx_init(){
	//48=0 en ASCII
	Rx_data[0]=48+9;
}

void Tx_null(){
	Tx_data[0]=48+0;//0 ASCII
	Tx_data[1]=48+0;//0 ASCII
}*/

/*void trayectoria(){
	trayectoria_ok=1;
	Tx_data[0]='1';
	HAL_UART_Transmit(&huart3, Tx_data, 1,200);
	//while(trayectoria_ok==0){}*/

	/*Tx_data[0]='2';//2 ASCII
	Tx_data[1]='2';//2 ASCII
	HAL_UART_Transmit(&huart2, Tx_data, 2, 100);
	HAL_Delay(100);
	Tx_null();
	HAL_UART_Receive(&huart2, Rx_data, 8, 100);
	HAL_Delay(100);
	Rx_init();
	Tx_data[0]='1';//1 ASCII
	while (Rx_data[0]==48+9){//9 ASCII
		HAL_UART_Transmit(&huart2, Tx_data, 1, 100);
		HAL_Delay(100);
		HAL_UART_Receive(&huart2, Rx_data, 4, 100);
		HAL_Delay(100);
	}
	while (Rx_data[0]!=48+9){//9 ASCII
		HAL_UART_Transmit(&huart2, Tx_data, 1, 100);
		HAL_Delay(100);
		//48=0 en ASCII
		puntos[n][0]=(Rx_data[0]-48)*10+(Rx_data[1]-48);
		puntos[n][1]=(Rx_data[2]-48)*10+(Rx_data[3]-48);
		n++;
		HAL_UART_Receive(&huart2, Rx_data, 4, 100);
		HAL_Delay(100);
	}
	Tx_data[0]='9';//9 ASCII
	HAL_UART_Transmit(&huart2, Tx_data, 1, 200);
	HAL_Delay(100);

	Tx_null();
	HAL_UART_Transmit(&huart2, Tx_data, 2, 100);
	HAL_Delay(100);*/
//}*/

/*void posicion_actual(){
	Tx_data[0]='1';//1 ASCII
	Tx_data[1]='1';//1 ASCII
	HAL_UART_Transmit(&huart2, Tx_data, 2, 100);
	HAL_Delay(100);
	Tx_null();
	HAL_UART_Receive(&huart2, Rx_data, 8, 100);
	HAL_Delay(100);
	Rx_init();
	Tx_data[0]='1';//1 ASCII
	while (Rx_data[0]==48+9){//9 ASCII
		HAL_UART_Receive(&huart2, Rx_data, 10, 200);
		HAL_Delay(100);
	}
	while (Rx_data[0]!=48+9){//9 ASCII
		HAL_UART_Transmit(&huart2, Tx_data, 1, 100);
		HAL_Delay(100);
		//48=0 en ASCII
		posicion[0]=(Rx_data[0]-48)*10+(Rx_data[1]-48)+((float)Rx_data[3]-48)/10+((float)Rx_data[4]-48)/100;
		posicion[1]=(Rx_data[5]-48)*10+(Rx_data[6]-48)+((float)Rx_data[8]-48)/10+((float)Rx_data[9]-48)/100;
		HAL_UART_Receive(&huart2, Rx_data, 10, 200);
		HAL_Delay(100);
	}
	Tx_null();
	HAL_UART_Transmit(&huart2, Tx_data, 2, 100);
	HAL_Delay(100);
}*/


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

	if (huart->Instance==USART2){
		inicio=1;
		posicion[0]=(Rx_data2[0]-48)*10+(Rx_data2[1]-48)+((float)Rx_data2[2]-48)/10+((float)Rx_data2[3]-48)/100;
		posicion[1]=(Rx_data2[4]-48)*10+(Rx_data2[5]-48)+((float)Rx_data2[6]-48)/10+((float)Rx_data2[7]-48)/100;

		/*Tx_data[0]='0';
		HAL_UART_Transmit_DMA(&huart3, Tx_data, 1);*/
	}
	else if(huart->Instance==USART3){
		for(int i=0;i<Npuntos;i++) {
			if(Rx_data3[i*4]=='9'){
				break;
			}
			//48=0 en ASCII
			puntos[i][0]=(Rx_data3[i*4]-48)*10+(Rx_data3[i*4+1]-48);
			puntos[i][1]=(Rx_data3[i*4+2]-48)*10+(Rx_data3[i*4+3]-48);
			n=i+1;

		}
		trayectoria_ok=2;
		puntos[n][0]=puntos[n-1][0];
		puntos[n][1]=puntos[n-1][1];
		puntos[n+1][0]=puntos[n-1][0];
		puntos[n+1][1]=puntos[n-1][1];
	}
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_I2S3_Init();
  MX_SPI1_Init();
  MX_USB_HOST_Init();
  MX_TIM2_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_TIM1_Init();
  MX_USART3_UART_Init();
  /* USER CODE BEGIN 2 */
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2); // Start PWM canal 2
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1); // Start PWM canal 1
  TIM2->CCR2=108;
  HAL_Delay(100);
  TIM1->CCR1=106;
  HAL_Delay(100);

  HAL_UART_Receive_DMA (&huart3, Rx_data3, Npuntos*4);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  /**
   * Tx_data
   * 0 = nulo
   * 1 = posicion
   * 2 = planificar
   * */


  /*Rx_init();

  Tx_null();*/
  //trayectoria();

  //Tx_data[0]='1';
  while (1)
  {
    /* USER CODE END WHILE */
    MX_USB_HOST_Process();

    /* USER CODE BEGIN 3 */

    //HAL_UART_Transmit_DMA(&huart3, Tx_data, 1);
    HAL_UART_Receive_DMA (&huart2, Rx_data2, 8);
	  if(inicio==1){
		  /*if (trayectoria_ok==1){
		  for(int i=0;i<Npuntos;i++) {
			  if(Rx_data3[i*4]=='9'){
				  break;
			  }
				  //48=0 en ASCII
				  puntos[i][0]=(Rx_data3[i*4]-48)*10+(Rx_data3[i*4+1]-48);
				  puntos[i][1]=(Rx_data3[i*4+2]-48)*10+(Rx_data3[i*4+3]-48);
				  n=i+1;
				  trayectoria_ok=2;
		  }
	  }
	  Tx_data[0]='0';
	  HAL_UART_Transmit_DMA(&huart3, Tx_data, 1);*/

		  if (trayectoria_ok==2){

			  xd=puntos[numfilas][0];
			  yd=puntos[numfilas][1];

			  xdelta=xd-posicion[0];
			  ydelta=yd-posicion[1];

			  //esquina= (xd!=puntos[numfilas+1][0]) && (yd!=puntos[numfilas+1][1]);

			  if (xdelta>1.5){// && (yd==puntos[numfilas+1][1])){//} || ((xdelta>0.1) & esquina)){
				  if (esquina==0){
						  TIM1->CCR1=106;
						  TIM2->CCR2=108+5+tol;
						  HAL_Delay(delay);
						  TIM2->CCR2=108+1;
						  HAL_Delay(delay);

						  next_i=1;
						  while(next_i<10){
							  if (((puntos[numfilas+next_i][0]-posicion[0])>0.5)){
								  numfilas+=1;
								  break;
							  }
							  if ((fabs(posicion[0]-puntos[n][0])<2) && (fabs(posicion[1]-puntos[n][1])<2)){
								  break;
							  }
							  next_i+=1;
						  }
				  }
				  else{
				xanterior=posicion[0]; yanterior=posicion[1];
				  TIM2->CCR2=108+4+tol;
				  HAL_Delay(200);
				  }
				  //tol1+=1;
				  /*if (fabs(xd-xanterior)>fabs(xd-posicion[0])){
						  TIM2->CCR2=108-4;HAL_Delay(10);}*/
			  }
			  else if (xdelta<-1.5){// && (yd==puntos[numfilas+1][1])){// || ((xdelta<-0.1) & esquina)){
				  if (esquina==0){
					  TIM1->CCR1=106;
					  TIM2->CCR2=108-5-tol;
					  HAL_Delay(delay);
					  TIM2->CCR2=108-1;
					  HAL_Delay(delay);

					  next_i=1;
					  while(next_i<20){
						  if (((puntos[numfilas+next_i][0]-posicion[0])>0.5)){
							  numfilas+=1;
							  break;
						  }
						  if ((fabs(posicion[0]-puntos[n][0])<2) && (fabs(posicion[1]-puntos[n][1])<2)){
							  break;
						  }
						  next_i+=1;
					  }
				  }
				  else{
				  xanterior=posicion[0]; yanterior=posicion[1];
				  TIM2->CCR2=108-5-tol;
				  HAL_Delay(200);}
				  //tol2+=1;
				  /*if (fabs(xanterior-xd)>fabs(posicion[0]-xd)) {
						  TIM2->CCR2=108+3;HAL_Delay(10);}*/
			  }
			  if (ydelta>1.5){// && (xd==puntos[numfilas+1][0])) || ((ydelta>0.1) & esquina)){
				  if (esquina==0){
					  TIM2->CCR2=108;
					  TIM1->CCR1=106+5+tol;
					  HAL_Delay(delay);
					  TIM1->CCR1=106+1;
					  HAL_Delay(delay);

					  next_i=1;
					  while(next_i<10){
						  if (((puntos[numfilas+next_i][0]-posicion[0])>0.5)){
							  numfilas+=1;
							  break;
						  }
						  if ((fabs(posicion[0]-puntos[n][0])<2) && (fabs(posicion[1]-puntos[n][1])<2)){
							  break;
						  }
						  next_i+=1;
					  }
				  }
				   else{xanterior=posicion[0]; yanterior=posicion[1];
				  TIM1->CCR1=106+4+tol;
				  HAL_Delay(200);}
				  // tol3+=1;
				  /*if (fabs(yd-yanterior)>fabs(yd-posicion[1])){
						  TIM1->CCR1=106-3;HAL_Delay(10);}*/
			  }
			  else if (ydelta<-1.5){// && (xd==puntos[numfilas+1][0])){// || ((ydelta<-0.1) & esquina)){
				  if (esquina==0){
					  TIM2->CCR2=108;
					  TIM1->CCR1=106-4-tol;
					  HAL_Delay(delay);
					  TIM1->CCR1=106-1;
					  HAL_Delay(delay);

					  next_i=1;
					  while(next_i<10){
						  if (((puntos[numfilas+next_i][0]-posicion[0])>0.5)){
							  numfilas+=1;
							  break;
						  }
						  if ((fabs(posicion[0]-puntos[n][0])<2) && (fabs(posicion[1]-puntos[n][1])<2)){
							  break;
						  }
						  next_i+=1;
					  }
				  }
				  else{xanterior=posicion[0]; yanterior=posicion[1];
				  TIM1->CCR1=106-4-tol;
				  HAL_Delay(200);
				  }
				  //tol4+=1;
				  /*if (fabs(yanterior-yd)>fabs(posicion[1]-yd)) {
						  TIM1->CCR1=106+3;HAL_Delay(10);}*/
			  }
			  HAL_Delay(80);

			  if((fabs(xdelta)<2) && (fabs(ydelta)<2)){
				  numfilas+=1;
				  //TIM1->CCR1=106;TIM2->CCR2=108;
				  tol=0;tol2=0;
			  }

			  if ((tol2==50)){
				  numfilas+=1;
				  tol=0; tol2=0;
				  //TIM1->CCR1=106; TIM2->CCR2=108;

			  }

			  if (numfilas==n){
				  break;
			  }
			  tol2+=1;
			  if (tol<7)tol+=1;

			  if ((fabs(posicion[0]-puntos[n][0])<2) && (fabs(posicion[1]-puntos[n][1])<2)){
				  break;
			  }
			  /*else{
				  numfilas+=1;
				  //TIM1->CCR1=106;TIM2->CCR2=108;
				  tol=0;*/
		  }
		  if (esquina==1)
			  HAL_Delay(300);


	  }

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV4;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief I2S3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2S3_Init(void)
{

  /* USER CODE BEGIN I2S3_Init 0 */

  /* USER CODE END I2S3_Init 0 */

  /* USER CODE BEGIN I2S3_Init 1 */

  /* USER CODE END I2S3_Init 1 */
  hi2s3.Instance = SPI3;
  hi2s3.Init.Mode = I2S_MODE_MASTER_TX;
  hi2s3.Init.Standard = I2S_STANDARD_PHILIPS;
  hi2s3.Init.DataFormat = I2S_DATAFORMAT_16B;
  hi2s3.Init.MCLKOutput = I2S_MCLKOUTPUT_ENABLE;
  hi2s3.Init.AudioFreq = I2S_AUDIOFREQ_96K;
  hi2s3.Init.CPOL = I2S_CPOL_LOW;
  hi2s3.Init.ClockSource = I2S_CLOCK_PLL;
  hi2s3.Init.FullDuplexMode = I2S_FULLDUPLEXMODE_DISABLE;
  if (HAL_I2S_Init(&hi2s3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2S3_Init 2 */

  /* USER CODE END I2S3_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 1680;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 999;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 1680;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 4999;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Stream1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream1_IRQn);
  /* DMA1_Stream5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream5_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(CS_I2C_SPI_GPIO_Port, CS_I2C_SPI_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(OTG_FS_PowerSwitchOn_GPIO_Port, OTG_FS_PowerSwitchOn_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, LD4_Pin|LD3_Pin|LD5_Pin|LD6_Pin
                          |Audio_RST_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : CS_I2C_SPI_Pin */
  GPIO_InitStruct.Pin = CS_I2C_SPI_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(CS_I2C_SPI_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : OTG_FS_PowerSwitchOn_Pin */
  GPIO_InitStruct.Pin = OTG_FS_PowerSwitchOn_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(OTG_FS_PowerSwitchOn_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PDM_OUT_Pin */
  GPIO_InitStruct.Pin = PDM_OUT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
  HAL_GPIO_Init(PDM_OUT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : BOOT1_Pin */
  GPIO_InitStruct.Pin = BOOT1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(BOOT1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : CLK_IN_Pin */
  GPIO_InitStruct.Pin = CLK_IN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
  HAL_GPIO_Init(CLK_IN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LD4_Pin LD3_Pin LD5_Pin LD6_Pin
                           Audio_RST_Pin */
  GPIO_InitStruct.Pin = LD4_Pin|LD3_Pin|LD5_Pin|LD6_Pin
                          |Audio_RST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : OTG_FS_OverCurrent_Pin */
  GPIO_InitStruct.Pin = OTG_FS_OverCurrent_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(OTG_FS_OverCurrent_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : MEMS_INT2_Pin */
  GPIO_InitStruct.Pin = MEMS_INT2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(MEMS_INT2_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
