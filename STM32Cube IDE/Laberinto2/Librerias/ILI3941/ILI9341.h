#include "MYaqoob/ILI3941_init_with_logo.h"
#include "victorvano/MY_ILI9341.h"
#include "victorvano/TSC2046.h"

void Init_ILI9341(SPI_HandleTypeDef *spiLcdHandle, GPIO_TypeDef *csPORT, uint16_t csPIN, GPIO_TypeDef *dcPORT, uint16_t dcPIN, GPIO_TypeDef *resetPORT, uint16_t resetPIN);
void cargar_imagen(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint8_t *data);


