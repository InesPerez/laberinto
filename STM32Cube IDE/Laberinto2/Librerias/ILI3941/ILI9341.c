#include "ILI9341.h"

static SPI_HandleTypeDef lcdSPIhandle;
//Chip Select pin
static GPIO_TypeDef  *CS_GPIO;
static uint16_t CS_PIN;
//Data Command pin
static GPIO_TypeDef  *DC_GPIO;
static uint16_t DC_PIN;
//Reset pin
static GPIO_TypeDef  *RESET_GPIO;
static uint16_t RESET_PIN;

void Init_ILI9341(SPI_HandleTypeDef *spiLcdHandle, GPIO_TypeDef *csPORT, uint16_t csPIN, GPIO_TypeDef *dcPORT, uint16_t dcPIN, GPIO_TypeDef *resetPORT, uint16_t resetPIN){

	//Copy SPI settings
	memcpy(&lcdSPIhandle, spiLcdHandle, sizeof(*spiLcdHandle));
	//CS pin
	CS_GPIO = csPORT;
	CS_PIN = csPIN;
	//DC pin
	DC_GPIO = dcPORT;
	DC_PIN = dcPIN;
	//RESET pin
	RESET_GPIO = resetPORT;
	RESET_PIN = resetPIN;

	ILI9341_Init(spiLcdHandle, csPORT, csPIN, dcPORT, dcPIN, resetPORT, resetPIN);
}

//12. Image print (RGB 565, 2 bytes por pixel)
void cargar_imagen(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint8_t *data)
{

	uint32_t x_led = x;
	uint32_t y_led;

	for(uint32_t i=0; i<w ; i++)
	{
		y_led = ILI9341_HEIGHT-(y+i);
		ILI9341_SetCursorPosition(x_led, y_led, ILI9341_WIDTH, ILI9341_HEIGHT);
		//Set DC LOW for DATA mode
		HAL_GPIO_WritePin(DC_GPIO, DC_PIN, GPIO_PIN_SET);
		//Put CS LOW
		HAL_GPIO_WritePin(CS_GPIO, CS_PIN, GPIO_PIN_RESET);
		//Write byte using SPI
		HAL_SPI_Transmit(&lcdSPIhandle, (uint8_t *)&data[(h*w*2)-(2*h*(1+i))], 2*h,100);
		//Bring CS HIGH
		HAL_GPIO_WritePin(CS_GPIO, CS_PIN, GPIO_PIN_SET);
	}

}


